import datetime

from django.test import TestCase
from rest_framework.test import CoreAPIClient

from mailing.models import Client, Message, Mailing, Tag, Status, MobileCode


class PostPagesTests(TestCase):
    BASE_URL = 'http://127.0.0.1:1337/api/v1/'

    def setUp(self):
        super().setUpClass()
        self.client = CoreAPIClient()
        self.mobile_code = MobileCode.objects.create(
            name='919'
        )
        self.other_code = MobileCode.objects.create(
            name='900'
        )
        self.status = Status.objects.create(
            name='404'
        )
        self.tag = Tag.objects.create(
            name='Test tag'
        )
        self.other_tag = Tag.objects.create(
            name='Other tag'
        )
        self.mailing_client = Client.objects.create(
            phone='79168621356',
            mobile_code=self.mobile_code,
            tag=self.tag,
            time_zone='3'
        )
        self.other_client = Client.objects.create(
            phone='79167799676',
            mobile_code=self.mobile_code,
            tag=self.other_tag,
            time_zone='3'
        )
        self.mailing = Mailing.objects.create(
            start=datetime.datetime(2022, 4, 20, 22, 22, 22),
            text='Test mailing text',
            mobile_code=self.mobile_code,
            tag=self.tag,
            end=datetime.datetime(2022, 4, 26, 22, 22, 22)
            )
        self.message = Message.objects.create(
            status=self.status,
            mailing=self.mailing,
            client=self.mailing_client
        )
        self.clients_count = 2

    def test_pages_on_clients_contains_context(self):
        """Проверка содержания эндпоинта 'clients'."""
        response = self.client.get(self.BASE_URL + 'clients/')
        first_client = response[0]
        mailing_client_parametres = {
            'id': self.mailing_client.id,
            'phone': self.mailing_client.phone,
            'time_zone': self.mailing_client.time_zone,
            'mobile_code': self.mailing_client.mobile_code.pk,
            'tag': self.mailing_client.tag.pk,
            }
        self.assertEqual(first_client, mailing_client_parametres)
        self.assertEqual(len(response), self.clients_count)



