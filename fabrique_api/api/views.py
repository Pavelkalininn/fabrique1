from http import HTTPStatus

import requests
from django_filters.rest_framework import DjangoFilterBackend
from rest_framework import viewsets, filters
from rest_framework.decorators import action
from rest_framework.response import Response
from rest_framework.generics import get_object_or_404

from fabrique_api.settings import TOKEN
from mailing_runner.mailing_runner import app
from .serializers import (
    ClientSerializer, MailingSerializer, MessageSerializer)

from mailing.models import Client, Mailing, Message, Status

ENDPOINT = 'https://probe.fbrq.cloud/v1/send/'


@app.task(bind=True, default_retry_delay=10 * 60)
def send_message(self, message_id, phone, text):
    params = {"id": message_id,
              "phone": phone,
              "text": text}
    headers = {'Authorization': f'Bearer {TOKEN}'}
    status = requests.post(
        ENDPOINT + str(message_id), headers=headers, json=params)
    if status.status_code != HTTPStatus.OK:
        self.retry()
    return status.status_code


class ClientViewSet(viewsets.ModelViewSet):
    queryset = Client.objects.all()
    serializer_class = ClientSerializer


class MailingViewSet(viewsets.ModelViewSet):
    queryset = Mailing.objects.all()
    serializer_class = MailingSerializer
    filter_backends = (DjangoFilterBackend, filters.SearchFilter)

    @action(methods=['get', ], detail=False)
    def statistic(self, request):
        statistic_dict = {'messages': len(Message.objects.all())}

        for mailing in Mailing.objects.all():
            status_filter = {}
            for status in Status.objects.all():
                status_filter[status.__str__()] = len(
                    Message.objects.filter(status=status))
            statistic_dict[mailing.pk] = status_filter
        return Response(statistic_dict)


class MessageViewSet(viewsets.ModelViewSet):
    serializer_class = MessageSerializer

    def get_queryset(self):
        mailing_id = self.kwargs.get("mailing_id")
        new_queryset = Message.objects.filter(
            mailing=get_object_or_404(Mailing, pk=mailing_id))
        return new_queryset

    def perform_create(self, serializer):
        mailing = get_object_or_404(
                Mailing,
                pk=self.kwargs.get("mailing_id"))
        client = get_object_or_404(
                Client, pk=self.request.data.get('client'))
        message = serializer.save(mailing=mailing)
        status_code = send_message(
            message.id,
            client.phone,
            mailing.text
        )
        if Status.objects.filter(name=status_code).exists():
            status_obj = Status.objects.get(name=status_code)
        else:
            status_obj = Status.objects.create(name=status_code)
        serializer.save(mailing=mailing, status=status_obj)

    def perform_update(self, serializer):
        serializer.save(
            mailing=get_object_or_404(
                Mailing,
                pk=self.kwargs.get("mailing_id"))
        )
