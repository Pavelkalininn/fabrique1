from django.urls import path, include
from rest_framework import routers

from .views import ClientViewSet, MailingViewSet, MessageViewSet

router = routers.DefaultRouter()

router.register(r'clients', ClientViewSet, basename='client-list')
router.register(r'mailings', MailingViewSet, basename='mailing-list')
router.register(r'mailings\/(?P<mailing_id>[0-9]*)\/messages',
                MessageViewSet,
                basename='message-list'
                )

urlpatterns = [
    path('v1/', include(router.urls)),
]
