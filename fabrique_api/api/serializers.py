import re

from rest_framework import serializers
from rest_framework.validators import UniqueTogetherValidator

from mailing.models import Mailing, Client, Message


class MailingSerializer(serializers.ModelSerializer):

    class Meta:
        fields = '__all__'
        model = Mailing


class ClientSerializer(serializers.ModelSerializer):

    def validate_phone(self, data):
        if not re.fullmatch(r'7\d{10}', data['phone']):
            raise serializers.ValidationError(
                'Телефон должен начинаться с семерки и состоять из 11 цифр')
        return data

    class Meta:
        fields = '__all__'
        model = Client


class MessageSerializer(serializers.ModelSerializer):
    class Meta:
        model = Message
        fields = '__all__'
        validators = [
            UniqueTogetherValidator(
                queryset=Message.objects.all(),
                fields=('client', 'mailing')
            )
        ]
