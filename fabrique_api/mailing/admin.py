from django.contrib import admin

from .models import Message, Client, Mailing, MobileCode, Tag, Status


class MailingAdmin(admin.ModelAdmin):
    list_display = ('pk', 'start', 'text', 'mobile_code', 'tag', 'end')
    search_fields = ('text',)
    list_filter = ('start', 'text', 'mobile_code', 'tag', 'end')
    empty_value_display = '-пусто-'


admin.site.register(Mailing, MailingAdmin)
admin.site.register(Client)
admin.site.register(Message)
admin.site.register(MobileCode)
admin.site.register(Tag)
admin.site.register(Status)
