# Generated by Django 3.2.13 on 2022-04-22 21:09

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('mailing', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='client',
            name='time_zone',
            field=models.IntegerField(max_length=3, verbose_name='Часовой пояс'),
        ),
        migrations.AlterField(
            model_name='mobilecode',
            name='name',
            field=models.TextField(help_text='Введите код оператора', verbose_name='Код оператора'),
        ),
    ]
