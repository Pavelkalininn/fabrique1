from django.db import models


class Tag(models.Model):
    id = models.AutoField(primary_key=True)
    name = models.TextField(
        'Тэг',
        help_text='Введите тэг'
    )

    def __str__(self):
        return self.name.__str__()


class Status(models.Model):
    id = models.AutoField(primary_key=True)
    name = models.TextField(
        'Статус отправки',
        help_text='Введите статус отправки',
        unique=True
    )

    def __str__(self):
        return self.name.__str__()


class MobileCode(models.Model):
    id = models.AutoField(primary_key=True)
    name = models.TextField(
        'Код оператора',
        help_text='Введите код оператора'
    )

    def __str__(self):
        return self.name.__str__()


class Mailing(models.Model):
    id = models.AutoField(primary_key=True)
    start = models.DateTimeField(
        'Дата и время запуска рассылки',
    )
    text = models.TextField(
        'Текст сообщения',
        help_text='Введите текст сообщения'
    )
    mobile_code = models.ForeignKey(
        MobileCode,
        on_delete=models.SET_NULL,
        null=True,
        related_name='mailings',
        verbose_name='Код мобильного оператора',
        help_text='Укажите код мобильного оператора'
    )
    tag = models.ForeignKey(
        Tag,
        on_delete=models.SET_NULL,
        null=True,
        related_name='mailings_tag',
        verbose_name='Тэг',
        help_text='Укажите тэг'
    )
    end = models.DateTimeField(
        'Дата и время окончания рассылки',
    )


class Client(models.Model):
    id = models.AutoField(primary_key=True)
    phone = models.CharField(max_length=11)
    mobile_code = models.ForeignKey(
        MobileCode,
        on_delete=models.SET_NULL,
        null=True,
        related_name='clients',
        verbose_name='Код мобильного оператора',
        help_text='Укажите код мобильного оператора'
    )
    tag = models.ForeignKey(
        Tag,
        on_delete=models.SET_NULL,
        null=True,
        related_name='clients',
        verbose_name='Тэг',
        help_text='Укажите тэг'
    )
    time_zone = models.TextField(
        'Часовой пояс',
    )


class Message(models.Model):
    id = models.AutoField(primary_key=True)
    create = models.DateTimeField(
        'Дата создания',
        auto_now_add=True
    )
    status = models.ForeignKey(
        Status,
        on_delete=models.SET_NULL,
        null=True,
        related_name='messages',
        verbose_name='Статус',
        help_text='Укажите статус'
    )
    mailing = models.ForeignKey(
        Mailing,
        on_delete=models.CASCADE,
        related_name='messages',
        verbose_name='Рассылка'
    )
    client = models.ForeignKey(
        Client,
        on_delete=models.CASCADE,
        related_name='messages',
        verbose_name='Клиент'
    )

    class Meta:
        ordering = ['-status']
        constraints = [
            models.UniqueConstraint(
                fields=['client', 'mailing'],
                name='unique_message'),
        ]
