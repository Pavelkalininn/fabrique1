from django.apps import AppConfig


class MailingRunnerConfig(AppConfig):
    name = 'mailing_runner'
