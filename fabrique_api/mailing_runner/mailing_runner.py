import os
from datetime import datetime
from http import HTTPStatus

from celery import Celery

os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'fabrique_api.settings')
app = Celery('mailing_runner', backend='redis://localhost', broker='pyamqp://')

app.config_from_object('django.conf:settings', namespace='CELERY')
app.autodiscover_tasks()

SEND_STATUS = '000'


@app.on_after_configure.connect
def setup_periodic_tasks(sender, **kwargs):
    sender.add_periodic_task(1.0, runner.s(), name='run every second')


@app.task
def runner():
    from mailing.models import Mailing, Client, Message, Status
    now = datetime.now()
    for mailing in Mailing.objects.filter(
            start__lte=now, end__gte=now):
        if mailing:
            for client in Client.objects.filter(
                    mobile_code=mailing.mobile_code,
                    tag=mailing.tag):
                if Message.objects.filter(
                        mailing=mailing, client=client).exclude(
                        status=HTTPStatus.OK).exists():
                    if Status.objects.filter(name=SEND_STATUS).exists():
                        status_obj = Status.objects.get(name=SEND_STATUS)
                    else:
                        status_obj = Status.objects.create(name=SEND_STATUS)
                    Message.objects.save(status=status_obj,
                                         mailing_id=mailing.id,
                                         client=client)
